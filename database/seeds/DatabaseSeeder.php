<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CompaniesTableSeeder::class); //must be first
        $this->call(UsersSeeder::class);
        $this->call(DoctorsTableSeeder::class);
    }
}
