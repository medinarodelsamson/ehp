<?php

use Illuminate\Database\Seeder;

use App\Company;

class DoctorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('doctors')->insert([
            'first_name' => 'John Smith',
            'last_name' => 'su@example.com',
            'company_id' => Company::first()->id
        ]);
    }
}
