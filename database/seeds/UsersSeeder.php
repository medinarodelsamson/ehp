<?php

use Illuminate\Database\Seeder;
use App\Company;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'John Smith',
            'email' => 'su@example.com',
            'password' => bcrypt('secret'),
            'api_token' => str_random(60),
            'role' => 0,
            'company_id' => Company::first()->id
        ]);
    }
}
