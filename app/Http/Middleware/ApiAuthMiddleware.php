<?php

namespace App\Http\Middleware;
use App\User;
use Closure;

class ApiAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::whereApiToken($request->header('api_token'))->first();

        if ($user) {
            $request->user = $user;
            $request->company = $user->company;
        }else{
            return response()->json(['errors' => 'Unauthorized'], '401');
        }

        return $next($request);
    }

}
