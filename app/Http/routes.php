<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace' => 'api', 'prefix' => 'api'], function () {

  # api/v1/auth
  Route::group(['namespace' => 'v1', 'prefix' => 'v1'], function () {
      # ^/auth
      Route::resource('auth', 'ApiAuthController');

      Route::group(['middleware' => ['api_auth_middleware']], function () {
        # ^/doctors
        Route::resource('doctors', 'ApiDoctorsController');
        # ^/patients
        Route::resource('patients', 'ApiPatientsController');
      });
  });

});


# admin/api/v1