<?php
namespace App\Http\Controllers\Api\V1;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Patient;

class ApiPatientsController extends Controller {
  public function index(Request $request)
  {
    return response()->json(Patient::whereCompanyId($request->company->id)->get());
  }
}