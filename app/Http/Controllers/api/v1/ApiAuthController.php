<?php
namespace App\Http\Controllers\Api\V1;

use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

use App\User;

class ApiAuthController extends Controller {
    public function store(Request $request)
    {
        $user = User::whereEmail($request->email)->first();

        if ($user) {
          if (Hash::check($request->password, $user->password)) {
            return response()->json($user);
          }else{
            return $this->error_response(["Bad credentials"]);
          }

        }else{
          return $this->error_response(["Not found"]);
        }
        
    }
}