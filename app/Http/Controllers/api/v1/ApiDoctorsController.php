<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Doctor;

class ApiDoctorsController extends Controller {
  public function index(Request $request)
  {
    return response()->json(Doctor::whereCompanyId($request->company->id)->get());
  }
}