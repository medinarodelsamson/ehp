<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function doctors(){
        return $this->hasMany('App\Doctor');
    }

    public function patients(){
        return $this->hasMany('App\Patient');
    }

    public function users(){
        return $this->hasMany('App\Patient');
    }
}